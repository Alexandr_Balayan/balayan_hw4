package app;

import java.util.Scanner;
import java.util.Random;

public class homework4 {
    public static void main(String[] args) {
        Random rand = new Random();
        int startNumber = 1 + rand.nextInt(8); //выбрал рандомную первую цифру от 1 до 8 включительно
        int attempts = 3; //количество попыток
        System.out.println("Выберите уровень сложности - 1 или 2?");
        Scanner input = new Scanner(System.in);
        int level = input.nextInt();
        if (level == 1) {
            System.out.print(startNumber + " ");
            for (int i = 0; i < 6; i++) {
                startNumber += 3;
                System.out.print(startNumber + " ");
            }
            System.out.println("");
            int answer = startNumber + 3;
            int userAttempts = 1;
            while (userAttempts <= attempts) {
                System.out.println("Введите следующее число:");
                int userAnswer = input.nextInt();
                if (userAnswer == answer) {
                    System.out.println("Ответ правильный!");
                    break;
                } else if (userAttempts != attempts) {
                    System.out.println("Неправильный ответ, попробуй еще раз:");
                } else {
                    System.out.println("Неправильный ответ, попытки закончились.");
                    System.out.println("Правильный ответ: " + answer);
                }
                userAttempts += 1;
            }
        } else if (level == 2) {
            System.out.print(startNumber + " ");
            for (int i = 0; i < 3; i++) {
                startNumber += 5;
                System.out.print(startNumber + " ");
                startNumber *= 2;
                System.out.print(startNumber + " ");
                startNumber -= 7;
                System.out.print(startNumber + " ");
            }
            int answer = startNumber + 5;
            int userAttempts = 1;
            while (userAttempts <= attempts) {
                System.out.println("Введите следующее число:");
                int userAnswer = input.nextInt();
                if (userAnswer == answer) {
                    System.out.println("Ответ правильный!");
                    break;
                } else if (userAttempts != attempts) {
                    System.out.println("Неправильный ответ, попробуй еще раз:");
                } else {
                    System.out.println("Неправильный ответ, попытки закончились.");
                    System.out.println("Правильный ответ: " + answer);
                }
                userAttempts += 1;
            }
        } else {
            System.out.println("Ты ввел какую-то фигню, перезапусти программу");
        }
    }
}


